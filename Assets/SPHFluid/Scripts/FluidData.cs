﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Fireworks {
    [CreateAssetMenu (menuName = "Fluid Data ")]
    public class FluidData : ScriptableObject {

        public float smoothlen = 0.012f;                                // 粒子半径
        public float pressureStiffness = 200.0f;                          // 圧力項係数
        public float restDensity = 1000.0f;                             // 静止密度
        public float particleMass = 0.0002f;                            // 粒子質量
        public float particleDist = 170.9976f;                          // 粒子の初期間隔
        public float viscosity = 0.1f;                                  // 粘性係数
        public float maxAllowableTimestep = 0.005f;                     // 時間刻み幅
        public float wallStiffness = 3000.0f;                           // ペナルティ法の壁の力


        [Header ("Heat")]
        [SerializeField]
        public float starRadius = 0.5f;                                 // 星の半径   
        public float buoyancyCoef = 3.0f;                               // 浮力係数   

        public float ballRadius = 0.5f;                                 // 粒子位置初期化時の円半径
    }
}
